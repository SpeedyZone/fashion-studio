// Ordinarily, you'd generate this data from markdown files in your
// repo, or fetch them from a database of some kind. But in order to
// avoid unnecessary dependencies in the starter template, and in the
// service of obviousness, we're just going to leave it here.

// This file is called `_posts.js` rather than `posts.js`, because
// we don't want to create an `/blog/posts` route — the leading
// underscore tells Sapper not to do that.

import * as contentful from 'contentful'
import { documentToHtmlString } from "@contentful/rich-text-html-renderer";

let client = contentful.createClient({
  space: `es1nuyuxptka`,
	accessToken: `zYGl8Q-Y-p46I2T4NeYtQP9JMhG1ZSeoPyd0IONfaXo`
})

const getPosts = () => client.getEntries().then((entries) => {
	let posts = [];
	// let entries = JSON.parse(res.stringifySafe()).entries
	entries.items.forEach((entry, i) => {
		posts = [...posts, entry.fields]
		// Object.keys(posts[i]).forEach(key => {
		// 	posts[i][key] = posts[i][key]['en-US']
		// })
		posts[i].content = documentToHtmlString(entry.fields.content)
	})
	// console.log(posts)
	return posts
})

export default getPosts